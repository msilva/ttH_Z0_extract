name=$1

#only need a fine 2D scan
for i in `seq 1 8`;
do   
    echo $i
    root -l -b -q macro/make_lep_RNN.C\($i,\"${name}\"\)
done
root -l -b -q macro/opti_RNN_lep.C\(1,\"${name}\"\)
root -b -q macro/plotter.C\(\"leptonic\",\"${name}\",\"RNN\"\)