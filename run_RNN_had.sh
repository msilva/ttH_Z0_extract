name=$1

#first run course 2D scan
for i in `seq 1 8`;
do   
    echo $i
    root -l -b -q macro/make_had_RNN.C\($i\)
done
root -l -b -q macro/opti_RNN_had.C\(1\)
root -b -q macro/plotter.C\(\"hadronic\",\"${name}\",\"RNN\"\)

#then run finer 5D scan
for i in `seq 1 8`;
do
    echo $i
    root -l -b -q macro/make_had_RNN_fine.C\($i\)
done
root -l -b -q macro/opti_RNN_had.C\(2\) >> cat_had_RNN_${name}.txt