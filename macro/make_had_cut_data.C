#include "aux/header_orig.h"
double lumi = 1;
void TH1s_hadronic(TString inputFileName, bool isMC){

  cout << inputFileName << endl;
  TChain *chain=MakeChain("output", inputFileName, "badfile", inputFileName.Contains(".root"));
  c = new output(chain);

  //kinematics
  TH1F * h_SFs = new TH1F("h_SFs", "h_SFs", 2, 0, 2);
  TH1F * h_tH1_TITI_myy = new TH1F("h_tH1_TITI_myy", "h_tH1_TITI_myy", 55, 105, 160);
  TH1F * h_tH1_NTNI_myy = new TH1F("h_tH1_NTNI_myy", "h_tH1_NTNI_myy", 55, 105, 160);
  TH1F * h_tH2_TITI_myy = new TH1F("h_tH2_TITI_myy", "h_tH2_TITI_myy", 55, 105, 160);
  TH1F * h_tH2_NTNI_myy = new TH1F("h_tH2_NTNI_myy", "h_tH2_NTNI_myy", 55, 105, 160);
  TH1F * h_ttH1_TITI_myy = new TH1F("h_ttH1_TITI_myy", "h_ttH1_TITI_myy", 55, 105, 160);
  TH1F * h_ttH1_NTNI_myy = new TH1F("h_ttH1_NTNI_myy", "h_ttH1_NTNI_myy", 55, 105, 160);
  TH1F * h_ttH2_TITI_myy = new TH1F("h_ttH2_TITI_myy", "h_ttH2_TITI_myy", 55, 105, 160);
  TH1F * h_ttH2_NTNI_myy = new TH1F("h_ttH2_NTNI_myy", "h_ttH2_NTNI_myy", 55, 105, 160);
  TH1F * h_ttH3_TITI_myy = new TH1F("h_ttH3_TITI_myy", "h_ttH3_TITI_myy", 55, 105, 160);
  TH1F * h_ttH3_NTNI_myy = new TH1F("h_ttH3_NTNI_myy", "h_ttH3_NTNI_myy", 55, 105, 160);
  TH1F * h_ttH4_TITI_myy = new TH1F("h_ttH4_TITI_myy", "h_ttH4_TITI_myy", 55, 105, 160);
  TH1F * h_ttH4_NTNI_myy = new TH1F("h_ttH4_NTNI_myy", "h_ttH4_NTNI_myy", 55, 105, 160);

  //run through the loop
  int nevt=c->fChain->GetEntries();
  cout << "Number of events: " << nevt << endl;  
  for(int ievt=0;ievt<nevt;ievt++){
    if(ievt%10000 == 0 ){cout<< "Event number: " << ievt << endl;}
    get_event(ievt);

    //define weight
    double wt = 1;
    if(isMC)
      wt = lumi*c->weight;

    //only care about hadronic 
    if(c->N_lep > 0) continue;

    //passes TITI or NTNI
    Bool_t TITI = bool(c->flag_passedPID) && bool(c->flag_passedIso);
    Bool_t NTNI = !TITI;
 
    //passes mass cuts
    if(isMC && fabs(c->mass_yy - 125000) > 2000)
      continue;

    Bool_t SR = false; 
    if(c->mass_yy > 120000 && c->mass_yy < 130000) 
      SR = true;
    
    //fill scale factor, same as in ML
    if(c->N_bjet30_fixed70 > 0 && c->N_lep == 0 && !SR) {    
      if(TITI) h_SFs->Fill(0.5,1);
      if(NTNI) h_SFs->Fill(1.5,1);
    }

    if(c->N_lep > 0) continue;

    //passes tH/ ttH selections  
    Bool_t passes = false, passes_tH = false;
    //passes tH 4j1b
    passes_tH = (c->N_jet35_cen == 4) && (c->N_bjet35_fixed70 == 1);
    if(passes_tH) {
      if(TITI) h_tH1_TITI_myy->Fill(c->mass_yy/1000., wt);
      if(NTNI) h_tH1_NTNI_myy->Fill(c->mass_yy/1000., wt);
    continue;}

    //passes tH 4j2b 
    passes_tH = (c->N_jet35_cen == 4) && (c->N_bjet35_fixed70 >= 2);
    if(passes_tH){
      if(TITI) h_tH2_TITI_myy->Fill(c->mass_yy/1000., wt);
      if(NTNI) h_tH2_NTNI_myy->Fill(c->mass_yy/1000., wt);
    continue;}

    //passes ttH 5j1b
    passes = (c->N_jet25_cen == 5) && (c->N_bjet25_fixed70 == 1);
    if(passes){
      if(TITI) h_ttH1_TITI_myy->Fill(c->mass_yy/1000., wt);
      if(NTNI) h_ttH1_NTNI_myy->Fill(c->mass_yy/1000., wt);
      continue;}
    
    //passes ttH 5j2b
    passes = (c->N_jet25_cen == 5) && (c->N_bjet25_fixed70 >= 2);
    if(passes){
      if(TITI) h_ttH2_TITI_myy->Fill(c->mass_yy/1000., wt);
      if(NTNI) h_ttH2_NTNI_myy->Fill(c->mass_yy/1000., wt);
      continue;}

    //passes ttH 6j1b 
    passes = (c->N_jet25_cen >= 6) && (c->N_bjet25_fixed70 == 1);
    if(passes){
      if(TITI) h_ttH3_TITI_myy->Fill(c->mass_yy/1000., wt);
      if(NTNI) h_ttH3_NTNI_myy->Fill(c->mass_yy/1000., wt);
      continue;}

    //passes ttH 6j2b
    passes = (c->N_jet25_cen >= 6) && (c->N_bjet25_fixed70 >= 2);
    if(passes){
      if(TITI) h_ttH4_TITI_myy->Fill(c->mass_yy/1000., wt);
      if(NTNI) h_ttH4_NTNI_myy->Fill(c->mass_yy/1000., wt);
      continue;}
  }
  
  //make root files
  TString file_name = inputFileName;
  file_name.ReplaceAll("all_inputs/","");
  file_name.ReplaceAll(".root","");
  TFile f("root_files/kin_th1s_"+file_name+"_hadronic_cut_based.root", "recreate");
  h_SFs->Write();
  h_tH1_TITI_myy->Write();
  h_tH1_NTNI_myy->Write();
  h_tH2_TITI_myy->Write();
  h_tH2_NTNI_myy->Write();
  h_ttH1_TITI_myy->Write();
  h_ttH1_NTNI_myy->Write();
  h_ttH2_TITI_myy->Write();
  h_ttH2_NTNI_myy->Write();
  h_ttH3_TITI_myy->Write();
  h_ttH3_NTNI_myy->Write();
  h_ttH4_TITI_myy->Write();
  h_ttH4_NTNI_myy->Write();

  f.Close();

  //delete junk
  delete h_SFs;
  delete h_tH1_TITI_myy;
  delete h_tH1_NTNI_myy;
  delete h_tH2_TITI_myy;
  delete h_tH2_NTNI_myy;
  delete h_ttH1_TITI_myy;
  delete h_ttH1_NTNI_myy;
  delete h_ttH2_TITI_myy;
  delete h_ttH2_NTNI_myy;
  delete h_ttH3_TITI_myy;
  delete h_ttH3_NTNI_myy;
  delete h_ttH4_TITI_myy;
  delete h_ttH4_NTNI_myy;

  delete chain;
  delete c;
}


int make_had_cut_data(){

  bool is_MC;
  TH1s_hadronic("all_inputs/data.root", is_MC = false);
  TH1s_hadronic("all_inputs/ttH.root", is_MC = true);
  TH1s_hadronic("all_inputs/tHjb_plus1.root", is_MC = true);
  TH1s_hadronic("all_inputs/tWH_plus1.root", is_MC = true);
  TH1s_hadronic("all_inputs/ggH.root", is_MC = true);
  TH1s_hadronic("all_inputs/VBF.root", is_MC = true);
  TH1s_hadronic("all_inputs/WH.root", is_MC = true);
  TH1s_hadronic("all_inputs/ZH.root", is_MC = true);

  return 0;
}
