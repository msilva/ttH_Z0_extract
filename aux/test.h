//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Jul  2 15:59:03 2017 by ROOT version 5.34/36
// from TTree test/test
// found on file: hadronic/data_hadronic_test.root
//////////////////////////////////////////////////////////

#ifndef test_h
#define test_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class test {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Float_t         m_yy;
   Float_t         mass_yy;
   Int_t           N_lep;
   Int_t           N_jet30;
   Int_t           N_jet30_cen;
   Int_t           N_bjet30;
   Float_t         HT_jet30;
   Float_t         mass_jet30;
   Float_t         pTt_yy;
   Float_t         ph_delt_eta2_1;
   Float_t         sigmet_TST;
   Int_t           ph_isTight1;
   Int_t           ph_iso1;
   Int_t           ph_isTight2;
   Int_t           ph_iso2;
   Int_t           flag_passedIso;
   Int_t           flag_passedPID;
   Float_t         weight;
   Float_t         random_number;
   Float_t         nnweight;

   // List of branches
   TBranch        *b_m_yy;   //!
   TBranch        *b_mass_yy;   //!
   TBranch        *b_N_lep;   //!
   TBranch        *b_N_jet30;   //!
   TBranch        *b_N_jet30_cen;   //!
   TBranch        *b_N_bjet30;   //!
   TBranch        *b_HT_jet30;   //!
   TBranch        *b_mass_jet30;   //!
   TBranch        *b_pTt_yy;   //!
   TBranch        *b_ph_delt_eta2_1;   //!
   TBranch        *b_sigmet_TST;   //!
   TBranch        *b_ph_isTight1;   //!
   TBranch        *b_ph_iso1;   //!
   TBranch        *b_ph_isTight2;   //!
   TBranch        *b_ph_iso2;   //!
   TBranch        *b_flag_passedIso;   //!
   TBranch        *b_flag_passedPID;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_random_number;   //!
   TBranch        *b_nnweight;   //!

   test(TTree *tree=0);
   virtual ~test();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef test_cxx
test::test(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("hadronic/data_hadronic_test.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("hadronic/data_hadronic_test.root");
      }
      f->GetObject("test",tree);

   }
   Init(tree);
}

test::~test()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t test::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t test::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void test::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("m_yy", &m_yy, &b_m_yy);
   fChain->SetBranchAddress("mass_yy", &mass_yy, &b_mass_yy);
   fChain->SetBranchAddress("N_lep", &N_lep, &b_N_lep);
   fChain->SetBranchAddress("N_jet30", &N_jet30, &b_N_jet30);
   fChain->SetBranchAddress("N_jet30_cen", &N_jet30_cen, &b_N_jet30_cen);
   fChain->SetBranchAddress("N_bjet30", &N_bjet30, &b_N_bjet30);
   fChain->SetBranchAddress("HT_jet30", &HT_jet30, &b_HT_jet30);
   fChain->SetBranchAddress("mass_jet30", &mass_jet30, &b_mass_jet30);
   fChain->SetBranchAddress("pTt_yy", &pTt_yy, &b_pTt_yy);
   fChain->SetBranchAddress("ph_delt_eta2_1", &ph_delt_eta2_1, &b_ph_delt_eta2_1);
   fChain->SetBranchAddress("sigmet_TST", &sigmet_TST, &b_sigmet_TST);
   fChain->SetBranchAddress("ph_isTight1", &ph_isTight1, &b_ph_isTight1);
   fChain->SetBranchAddress("ph_iso1", &ph_iso1, &b_ph_iso1);
   fChain->SetBranchAddress("ph_isTight2", &ph_isTight2, &b_ph_isTight2);
   fChain->SetBranchAddress("ph_iso2", &ph_iso2, &b_ph_iso2);
   fChain->SetBranchAddress("flag_passedIso", &flag_passedIso, &b_flag_passedIso);
   fChain->SetBranchAddress("flag_passedPID", &flag_passedPID, &b_flag_passedPID);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("random_number", &random_number, &b_random_number);
   fChain->SetBranchAddress("nnweight", &nnweight, &b_nnweight);
   Notify();
}

Bool_t test::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void test::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t test::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef test_cxx
