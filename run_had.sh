name=$1

#first run course 2D scan
for i in `seq 1 8`;
do   
    echo $i
    root -l -b -q macro/make_had_NN.C\($i,\"${name}\"\)
done
root -l -b -q macro/opti_had.C\(1,\"${name}\"\)
root -b -q macro/plotter.C\(\"hadronic\",\"${name}\",\"RNN\"\)

#then run finer 5D scan
for i in `seq 1 8`;
do
    echo $i
    root -l -b -q macro/make_had_NN_fine.C\($i,\"${name}\"\)
done
root -l -b -q macro/opti_had.C\(2,\"${name}\"\) >> cat_had_${name}.txt

#clean up
#rm temp_had.txt